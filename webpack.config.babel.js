import path from 'path';

const config = {
  entry: './assets/scripts/index.js',
  output: {
    path: path.resolve(__dirname, 'build'),
    publicPath: 'http://localhost:8080/build/',
    filename: 'bundle.js'
  },
  module: {
    loaders: [{
      test: /\.js$/,
      exclude: /(node_modules)/,
      loader: 'babel-loader'
    }, {
      test: /\.css$/,
      exclude: /(node_modules)/,
      use: [{
        loader: 'style-loader'
      }, {
       loader: 'css-loader',
       options: {
         minimize: true,
         sourceMap: true
       }
     }]
   }]
 }
};

export default config;
