import searchProductsByPrice from './helpers/search_products_by_price';
import searchProductsByType from './helpers/search_products_by_type';
import searchProductById from './helpers/search_product_by_id';
import searchAllProducts from './helpers/search_all_products';

import Catalog from './db/catalog';

// add event handlers here
document.getElementById('searchById').addEventListener('click', () =>
  processSearch(Number(document.getElementsByClassName('searchById')[0].value))
);

document.getElementById('searchByType').addEventListener('click', () =>
  processSearchProductsByType(
    document.getElementsByClassName('searchByType')[0].value
  )
);

document.getElementById('searchByPrice').addEventListener('click', () =>
  processSearchProductsByPrice(
    Number(document.getElementsByClassName('searchByPrice')[0].value)
  )
);

searchAllProducts(Catalog).then(products => updateTable('allTable', products));

// add function definitions here
function processSearch(searchId) {
  return searchProductById(searchId, Catalog).then(product =>
    Promise.all([
      searchProductsByPrice(product.price, 50, Catalog),
      searchProductsByType(product.type, Catalog),
      product
      ])).then(product => {
        const similarArray = getIntersection(
          product[0], product[1], product[2].id
        );
        updateExaminedText(product[2]);
        updateTable('similarTable', similarArray);
      }).catch(product => alert(product));
}

function processSearchProductsByType(searchType) {
  return searchProductsByType(searchType, Catalog).then(products => {
    updateTable('similarTable', products);
  }).catch(products => alert(products));
}

function processSearchProductsByPrice(searchPrice) {
  return searchProductsByPrice(searchPrice, 50, Catalog).then(products => {
    updateTable('similarTable', products);
  }).catch(products => alert(products));
}

function getIntersection(arrA, arrB, searchedId) {
  const samePrice = arrA;
  const sameType = arrB;
  const similarArray = [];
  samePrice.forEach(obj1 => {
    sameType.forEach(obj2 => {
      if(obj1.id == obj2.id && obj1.id != searchedId)
        similarArray.push(obj1);
    });
  });

  return similarArray;
}

function updateExaminedText(product) {
  const {id, price, type} = product;
  const outputString = `
  Product Id: ${id}
  Price: ${price}
  Type: ${type}
`;
  document.getElementById('productText').innerHTML = outputString;
}

function createTableHeader(tableId) {
  const columns = ['ProductId', 'Type', 'Price', 'Examine'];
  const tableHeader = columns.map(column =>
    `<th>${column}</th>`
  );
  return document.getElementById(tableId).innerHTML =
    `<tr>${tableHeader.join('')}</tr>`;
}

function updateTable(tableId, products) {
  const tableBody = document.getElementById(tableId);
  // reset table
  while (tableBody.hasChildNodes()) {
    tableBody.removeChild(tableBody.firstChild);
  }
  // create table header
  createTableHeader(tableId);

  const productList = products.map(product => {
    const tr = document.createElement('tr');
    const td1 = document.createElement('td');
    const td2 = document.createElement('td');
    const td3 = document.createElement('td');
    const td4 = document.createElement('button');

    td4.addEventListener('click', () =>
      processSearch(product.id)
    );

    td1.appendChild(document.createTextNode(product.id));
    td2.appendChild(document.createTextNode(product.type));
    td3.appendChild(document.createTextNode(product.price));
    td4.appendChild(document.createTextNode('Examine'));
    tr.appendChild(td1);
    tr.appendChild(td2);
    tr.appendChild(td3);
    tr.appendChild(td4);

    return tableBody.appendChild(tr);
  });

  return productList.join('');
}
