const Catalog = createRandomCatalog(100);

function createRandomProduct() {
  const typeArray = ['Electronics', 'Book', 'Clothing', 'Food'];
  const price = Number((Math.random() * 500).toFixed(2));
  const type = typeArray[Math.floor(Math.random() * 4)];
  // const valid = Math.random() > .5 ? true : false;
  return {
    price,
    type
  };
}

function createRandomCatalog(num) {
  const Catalog = [];
  for(let id = 0; id < num; id++) {
    const product = createRandomProduct();
    const {price, type} = product;
    Catalog.push({
      id,
      price,
      type
    });
  }
  return Catalog;
}

export default Catalog;
