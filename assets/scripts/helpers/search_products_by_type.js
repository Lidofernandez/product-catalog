const PRODUCT_TYPES = ['Electronics', 'Book', 'Clothing', 'Food'];

export default function searchProductsByType(type, catalog) {
  const promise = new Promise((resolve, reject) => {
    if(!PRODUCT_TYPES.includes(type)) {
      return reject(`Invalid Type: ${type}`);
    }
    setTimeout(() => {
      const products = catalog.filter(product => product.type === type);
        resolve(products);
    }, 1000);
  });
  return promise;
}
