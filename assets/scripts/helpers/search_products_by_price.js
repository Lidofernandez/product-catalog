export default function searchProductsByPrice(price, difference, catalog) {
  const promise = new Promise((resolve, reject) => {
    if(!Number.isFinite(price)) {
      return reject(`Invalid Price: ${price}`);
    }
    setTimeout(() => {
      const products = catalog.filter(product =>
        Math.abs(product.price - price) < difference
      );
      resolve(products);
    }, 1000);
  });
  return promise;
}
