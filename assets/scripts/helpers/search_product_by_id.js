export default function searchProductById(id, catalog) {
  const promise = new Promise((resolve, reject) => {
    setTimeout(() => {
      const product = catalog.filter(product => product.id === id)[0];
      return product ? resolve(product) : reject(`Invalid ID: ${id}`);
    }, 1000);
  });
  return promise;
}
