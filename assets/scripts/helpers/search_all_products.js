export default function searchAllProducts(catalog) {
  const promise = new Promise(resolve => {
    setTimeout(() => {
      resolve(catalog);
    }, 1000);
  });
  return promise;
}
