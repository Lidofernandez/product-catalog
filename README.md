# Product catalog
### The user should see the following:
* A section labeled "Search Product by Id" that includes an input field and a button labeled "Search Product".
* A section labeled "Search Product by Type" that includes an input field and a button labeled "Search Type".
* A section labeled "Search Product by Price" that includes an input field and a button labeled "Search Price".
* A section labeled "Examined Product" that displays the Product Id, Type, and Price of the examined product.
* A section labeled "List of Similar Products" that has a table that displays a list of similar products. In addition to the product details, each table row should have a button labeled "Examined Product" that examines the selected product.
* A section labeled" List of All Products" that has a table that displays a list of all products. In addition to the product details, each table row should have a button labeled "Examined Product" that examines the selected product.

### The user should be able to do the following:
* Populate the Similar Products List with products of the same type by pressing the "Search Type" button and entering a valid product type in the corresponding input field.
* Populate the Similar Products List with products within $50 by pressing the "Search Price" button and entering a valid price in the corresponding input field.
* Populate the Examined Product section by pressing the "Search Product" button and entering a valid product id in the corresponding input field. This action will also populate the Similar Products List with products that have the same type and are within $50 of the examined product.
* Populate the Examined Product section by clicking on the "Examine" button in any of the table rows. This action will also populate the Similar Products List with products that have the same type and are within $50 of the examined product.
* Populate the List of all Products upon opening the web application.
* See an error alert when searching for an invalid id, type or price.
